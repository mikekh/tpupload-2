/**
 * @author Mike Khan
 */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function(require) {

    // A constructor for defining new Ketch
    function Ketch(options) {'use strict';
        this.state = options.state || "used";
        this.wheelSize = options.wheelSize || "large";
        this.color = options.color || "blue";
        //this.mizzen = options.mizzen || false;
        //this.keel = options.keel || false;
        this.topsail = options.topsail || true;
    }

    return Ketch;

});
