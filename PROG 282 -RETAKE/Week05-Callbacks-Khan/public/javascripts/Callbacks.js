

function square(value, func) {
    'use strict';
     func(value);
};
function calculate(value) {
    'use strict';
	$("#result").html("Square of " + value + " is: " + value * value);
};




$(document).ready(function() {
    "use strict";
    var result = 0;
	square(3, calculate);
});
