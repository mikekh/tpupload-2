var MyObject = (function() {
	function MyObject() {
		console.log("Constructor called.");
		$("#sendString").click(this.stringSender);
	}

	MyObject.prototype.readyCalled = function() {
		$("#readyCalled").html("Ready was called and myObjected created");
	};

	MyObject.prototype.stringSender = function() {
		console.log("stringSender called.");
		$("#stringHolder").html('just placed this in p tag id stringHolder');
	};
	return MyObject;
}());

$(document).ready(function() {
    var myObject = new MyObject();
    myObject.readyCalled();
});;