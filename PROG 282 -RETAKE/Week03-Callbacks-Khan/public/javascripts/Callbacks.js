
// declare feet per miles constant
var FEET_PER_MILE = 5280;

function callbackHandler(func, value1, value2) {
    'use strict';
    //return value1 + value2;
    return func(value1, value2);
};

function callback1(value1, value2) {
    'use strict';
    return value1 + value2;
};

function callback2(value1, value2) {
    'use strict';
    return value1 % value2;
};

function callback3(value1, value2) {
    'use strict';
    return  value2 * FEET_PER_MILE;
};


$(document).ready(function() {
    "use strict";
    var result = 0;
    
    result = callbackHandler(callback1, 6, 3);
    $("#myList").append("<li>" + result + "</li>");
    
    result = callbackHandler(callback2, 4, 3);
    $("#myList").append("<li>" + result + "</li>");
    
    result = callbackHandler(callback3, 4, 3);
    $("#myList").append("<li>" + result + "</li>");

});
