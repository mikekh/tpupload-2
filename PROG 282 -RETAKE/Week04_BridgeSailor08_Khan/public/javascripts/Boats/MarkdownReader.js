if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function(require) {

    var MarkdownReader = function() {'use strict';

    };

    MarkdownReader.prototype = {
    
        getReaderType: function() {'use strict';
            return 'MarkdownReader';
        },
        
 /*      readFile : function() {'use strict';
            return "readFile() in  MarkdownReader object called.";
        }
 */
	readFile : function() {'use strict';
			var markDownData = "# My Markdown This is my markdown file. It has a list in it: - Item01 - Item02 - Item03";
            return markDownData;
            
            //Charlie-- can't get following format to work:
            //var markDownData = # My Markdown

			//This is my markdown file. It has a list in it:

			//- Item01
			//- Item02
			//- Item03

        }

    };
    
    return new MarkdownReader();
});