if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function(require) {

    var JsonReader = function() {'use strict';

    };

    JsonReader.prototype = {
    
        getReaderType: function() {'use strict';
            return 'JsonReader';
        },
        
       readFile : function() {'use strict';
       		 var myJsonData = [
				    {
				        "firstName": "George",
				        "lastName": "Washington"
				    }, {
				        "firstName": "John",
				        "lastName": "Adams"
				    }, {
				        "firstName": "Thomas",      
				        "lastName": "Jefferson"
				    }
				];
				
       		JSON.stringify(myJsonData);
       		return myJsonData;
      }
    };
    
    return new JsonReader();
});

/*
		
 		readFile : function() {'use strict';
            return "readFile() in  JsonReader object called.";
        }

*/