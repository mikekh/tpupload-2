var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Week07InClassRoutes_Khan' });
});

/* Works
router.get('/read', function(request, response) {
    response.send({ "result": "The server reports success from Week07InClassRoute_Khan routes/index.js" });
});
*/

//prams get passed in request object
router.get('/read', function(request, response) {
var queryObject = request.query;
var queryAsString = JSON.stringify(request.query);
console.log("Read called: " + queryAsString);
response.send({ "firstName": queryObject.firstName + ' ' + queryObject.lastName});
});

module.exports = router;
