
var MyStack = (function() {
	var myArray = [];
	function MyStack() {
		'use strict';
		console.log("MyStack Constructor called.");
		
		//empty myArray because one of javascript quirks that when you do a new
		//and if an object already exits then it copies the data to the //existing object in to the new object 
		myArray = [];

	};
	
	MyStack.prototype.getStackLength = function(){
	'use strict';
		return myArray.length;
	};
		
	MyStack.prototype.pushStack = function(element){
	'use strict';
		return myArray.push(element)
	};
	
	MyStack.prototype.popStack = function(){
	'use strict';
		if(this.getStackLength() === 0){
			throw new TypeError('Cannot pop empty stack.');
		}
		else{
			return myArray.pop();
		};
		
		
	};
	
	MyStack.prototype.removeItem = function(element){
	'use strict';
		var elementIndex = myArray.indexOf(element);
		if ((elementIndex == myArray.length - 1) || (elementIndex == 0)){
			return false;
		};
		
		adjustStack(elementIndex);
		return true;
	};
	
	var adjustStack = function(startPos){
		var endPos = myArray.length - 1;
		
		for(; startPos < endPos; startPos++){
			myArray[startPos] = myArray[startPos + 1]
		};
		
		// remove the last element in the array
		myArray.pop();

	};

	
	MyStack.prototype.testPalindrome  = function(value) {
        'use strict';
		var strPush = value;
		var strPop = '';
		var x = 0;
		
		if(typeof strPush === "number") {
			strPush = strPush.toString();
		};
		
		strPush = stripWhiteSpace(strPush);
		strPush = strPush.toLowerCase();
		strPush = stripPunctuation(strPush);
		
		for(x=0; x < strPush.length; x++){
			myArray.push(strPush.charAt(x));
		};

		for(x=0; x < strPush.length; x++){
			strPop = strPop + myArray.pop();
		};
		
		if(strPush === strPop){
			return true;
		}
		else{
		
			return false;
		};

    };

	//MyStack.prototype.stripWhiteSpace = function(value) {
	var stripWhiteSpace = function(value) {
        'use strict';
        return String(value)
            .replace(/ /g, '')
            .replace(/\t/g, '')
            .replace(/\r/g, '')
            .replace(/\n/g, '');    
    };
	
	///MyStack.prototype.stripPunctuation = function (value) {
	var stripPunctuation = function (value) {
        'use strict';
        return String(value)
            .replace(/ /g, '')
            .replace(/\t/g, '')
            .replace(/\r/g, '')
            .replace(/\n/g, '');    
    };
	

	return MyStack;
}());

var MyQueue = (function() {
	'use strict';
	
	var myArray = [];
	function MyQueue() {
		'use strict';
		console.log("MyQueue Constructor called.");
		
		//empty myArray because one of javascript quirks that when you do a new
		//and if an object already exits then it copies the data to the //existing object in to the new object 
		myArray = [];
	};
	
	var defaultArray = function(){
		'use strict';
		myArray.push("alpha");
		myArray.push("bravo");
		myArray.push("Charlie");
		
		$("#qCount").html("Number items in the queue is: " + myArray.length);
};
	MyQueue.prototype.isEmpty = function(){
	'use strict';
		if( myArray.length > 0){
			return false;
		}
		else{
			return true;
		};
	};
		
	MyQueue.prototype.getQueueLength = function(){
	'use strict';
		return myArray.length;
};
	MyQueue.prototype.getFrontElement = function(){
		'use strict';
		return myArray[0];
};

	 MyQueue.prototype.getBackElement = function(){
		'use strict';
		return myArray[myArray.length - 1];
};

	MyQueue.prototype.enQueue = function(element){
		'use strict';
		return myArray.push(element);
};
	MyQueue.prototype.deQueue = function(){
	'use strict';
		//return myArray.pop(); // does not work returns the last element in the array.
		return myArray.shift();
		
};

	MyQueue.prototype.padNumber = function(numberToPad, width, padValue) {
        'use strict';
        padValue = padValue || '0';
        numberToPad = numberToPad + '';
        if (numberToPad.length >= width) {
            return numberToPad;
        } else {
            return new Array(width - numberToPad.length + 1).join(padValue) + numberToPad;
        }
    };
	
	return MyQueue;
}());

/*
$(document).ready(function() {
    var myQueue = new MyQueue();
	var queueElement;
	var qLength = 0;
	
	myQueue.enQueue("alpha");
	myQueue.enQueue("bravo ");
	myQueue.enQueue("charlie");
	$("#qFront").html("Front Element of myQueue is: " + myQueue.getFrontElement());
	$("#qBack").html("Back Element of myQueue is: " + myQueue.getBackElement());
	$("#qCount").html("Number of elements in myQueue is: " + myQueue.getQueueLength());
	

	while (myQueue.getQueueLength() > 0){
		qLength++;
		queueElement = myQueue.deQueue();
	};
	
	myQueue.enQueue("alpha");
	myQueue.enQueue("bravo ");
	myQueue.enQueue("charlie");
	
	qLength = myQueue.getQueueLength();
	for(x = 0; x < qLength; x++){
		queueElement = myQueue.deQueue();
	};
	
	for(x=0; x<100000; x++){
		queueElement = myQueue.padNumber(x+1, 6, 0);
		myQueue.enQueue("Item" + queueElement);
	};
	
	qLength = myQueue.getQueueLength();

	
	var myStack = new MyStack();
	var stackElement;
	var stackLength = 0;
	
	
	stackElement = myStack.popStack();
	
	myStack.pushStack("alpha");
	myStack.pushStack("bravo");
	stackLength = myStack.pushStack("charlie");

	stackElement = myStack.popStack();
	
	myStack.pushStack("charlie");
	myStack.pushStack("delta");
	myStack.pushStack("echo");
	
	myStack.removeItem("bravo");
	myStack.removeItem("alpha");
	myStack.removeItem("echo");
  
});;
*/