// JavaScript Document
// Mike Khan
// Completed 5/26/2013

//This very simple drag drop game, player just drags the images
//and drops it on box to see if there it is the right boxes for
//that image. If it is then the backgroud of the dropbox changes to
//white and the image can not be dragged. I am not very good at graphic
//design so I took some images and edited them in PhotoShop.
//I think this covers what you asked for in the assignment.
////
// The app initializes the images to draggable and dropboxes to droppable. 
//Image is constrained in outer container, when it is dropped 
//handleDropEvent() calls  matchFound() that checks if the image was dropped 
//in correct box, if true then numbCorrect is incremented and then a check 
//is made to see if all images have been drop in their correct boxes. If so  
//the game over message is displayed.
//
//I did find a problem with calling functions after this line of code in  handleDropEvent():
//
//	$("#" + imageID).droppable('disable');

	//NOTE: VERY STRANGE isGameOver() called from this position DOES NOT WORK
	//isGameOver();
//
//Don�t know what is causing it.

// First Image = box 2
// Second Image = box 4
// Third Image = box 1
// Fourth Image = box 3


//keep track of correct drop of images
var numbCorrect = 0;

function init() {
	setDragables();
	setDroppables();
}

//make images draggable
function setDragables() {
  $('#drag1').draggable(
  	{
		containment: '#dropContainer'
	}
  );
  
  $('#drag2').draggable(
  	{
		containment: '#dropContainer'
	}
  );
  
  $('#drag3').draggable(
  	{
		containment: '#dropContainer'
	}
  );
  
  $('#drag4').draggable(
  	{
		containment: '#dropContainer'
	}
  );
}

//make boxes droppable
function setDroppables() {
 
  $('#dropBox1').droppable( {
    drop: handleDropEvent
  } );
  
  $('#dropBox2').droppable( {
    drop: handleDropEvent
  } );
  
  $('#dropBox3').droppable( {
    drop: handleDropEvent
  } );
  
  $('#dropBox4').droppable( {
    drop: handleDropEvent
  } );
}


// image has be dropped
function handleDropEvent( event, ui ) {
  var draggable = ui.draggable;
  var imageID = draggable.attr('id');
  var boxID = this.getAttribute('id') ;

  //alert( 'The square with ID "' + imageID + '" was dropped onto ' + boxID);
  
  //if image dropped on the right square, change the background color of the drop box
  //and disable dragging of the image.
  if (matchFound( imageID, boxID )){ 
    numbCorrect = numbCorrect + 1;
	isGameOver();
	$("#" + boxID).css({'background-color': 'white'});
	$("#" + imageID).droppable('disable');
	
	//NOTE: VERY STRANGE isGameOver() called from this position DOES NOT WORK
	//isGameOver();
	
  }
}

function isGameOver(){
	if(numbCorrect > 3){
		//alert("game over");
		$("#gameGuess").html('Congratulations you guessed them all. GAME OVER!');
	}
}

//returns true if the image was dropped in the correct square
//otherwise returns false
var matchFound = function checkMatched( imageID, boxID ) {
  if ((imageID == 'drag1') && (boxID == 'dropBox2')){
	return true;
  }
  if ((imageID == 'drag2') && (boxID == 'dropBox4')){
	return true;
  }
  if ((imageID == 'drag3') && (boxID == 'dropBox1')){
	return true;
  }
  if ((imageID == 'drag4') && (boxID == 'dropBox3')){
	return true;
  }
   
  return false;
  
}

$(document).ready(function(){
	init();
	
});
