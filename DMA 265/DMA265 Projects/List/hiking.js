window.onload = function () {
   //Store the hiking id Object information
    var hiking = $("hiking");
	
	//Store an array of all h2 elements within the hiking Object
    var h2Elements = hiking.getElementsByTagName("h2");
    		    
    var h2Node;
	
    for (var i = 0; i < h2Elements.length; i++ ) {
    	//Iterate and store the Object information in the h2Node variable
		h2Node = h2Elements[i];
    	
    	// Attach event handler
    	h2Node.onclick = function () {
    		
    		var currentNode = this;         // h2 is the current h2Node object
    		
    		if (currentNode.getAttribute("class") == "plus") {
    			currentNode.setAttribute("class", "minus");	
				currentNode.style.color = "blue";
    		}
    		else {
    			currentNode.setAttribute("class", "plus");
				currentNode.style.color = "black";
    		}
    		if (currentNode.nextElementSibling.getAttribute("class") == "closed") {
    			currentNode.nextElementSibling.setAttribute("class", "open");
    		}
    		else {
    			currentNode.nextElementSibling.setAttribute("class", "closed");
    		} 
    	}
    }
}
var $ = function (id) {
	return document.getElementById(id);
}