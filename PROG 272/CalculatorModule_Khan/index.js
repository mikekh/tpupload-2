/**
 * @author Mike
 */

var Calc = (function() {'use strict';

	// Private variable
	var x = 0;
	var y = 0;

	// Constructor
	function Calc(initX, initY) {
		x = initX;
		y = initY;
	}
         

	// callback to do the actual calculations
	Calc.prototype.calculate = function(func) {
		return func(x, y);
	};


	return Calc;
})();

$(document).ready(function() {'use strict';

	var numb1 = 3;
	var numb2 = 7;
	var retVal;

	var compute = new Calc(numb1, numb2);

	retVal = compute.calculate(function(a, b) {
		return (a + b);
	});
	$("#add").html("Result of " + numb1 + " + " + numb2 +" = " + retVal);


	retVal = compute.calculate(function(a, b) {
		return (a - b);
	});
	$("#subtract").html("Result of " + numb1 + " - " + numb2 +" = " + retVal);

	retVal = compute.calculate(function(a, b) {
		return (a * b);
	});
	$("#multiple").html("Result of " + numb1 + " * " + numb2 +" = " + retVal);

	retVal = compute.calculate(function(a, b) {
		return (a / b);
	});
	$("#divide").html("Result of " + numb1 + " / " + numb2 +" = " + retVal);
});
