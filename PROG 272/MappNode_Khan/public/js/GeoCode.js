/**
 * @author Charlie Calvert
 * Released under the MIT License
 */

/*global google: false */
/*jshint devel: true, browser: true, jquery: true, strict: true */

var GeoCode = (function() {'use strict';

	var startPosition = null;
	var directions = null;
	var geocoder;
	var googleMap;

	function GeoCode(latitude, longitude) {
		initialize(latitude, longitude);
		directions = new Directions(googleMap);
	}

	var initialize = function(latitude, longitude) {
		geocoder = new google.maps.Geocoder();
		startPosition = new google.maps.LatLng(latitude, longitude);
		var mapOptions = {
			zoom : 8,
			center : startPosition,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
		var mapCanvas = $('#mapCanvas');
		googleMap = new google.maps.Map(mapCanvas[0], mapOptions);
		makeMarker(startPosition, "init at Seattle");
	}
	var makeMarker = function(initPosition, initTitleString) {
		var marker = new google.maps.Marker({
			map : googleMap,
			position : initPosition,
			title : initTitleString
		});
	}
	
	// drawFlag set 0 draws only the maekers, set to 1 directions as well
	var geoCode = function(initAddress, drawFlag) {
		geocoder.geocode({
			'address' : initAddress
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var position = results[0].geometry.location;
				var latititude = position.lat();
				var longitude = position.lng();
				var titleString = initAddress + "; Latitude: " + latititude + "; Longitude: " + longitude;
				$("#position").html(titleString);
				googleMap.setCenter(position);
				
				makeMarker(position);
				if (drawFlag === 1){
					directions.drawRoute(startPosition, position);
				}
				
			} else {
				alert('Geocode error: ' + status);
			}
		});
	};

	GeoCode.prototype.getDirections = function() {
		var userAddress = $('#cityName').val();
		geoCode(userAddress, 1);
	};

	//SHOW MARKER FOR THE CITIES IN THE LIST
	GeoCode.prototype.listMarkers = function(locations) {
		
		var address = locations[0].cityName;
		var i;
		
		// note: second pram of geoCode call disables the direction path
		for (i = 0; i < locations.length; i++) {
			address = locations[i].cityName;
			geoCode(address, 0);
			}
};



	
	GeoCode.prototype.getCoordinates = function() {	
		var map;	
		var address = $('#cityName').val();
		geocoder.geocode({
			
			'address' : address
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var pos = results[0].geometry.location;
				var latititude = pos.lat();
				var longitude = pos.lng();
				
				$('#latitude').val(latititude);
				$('#longitude').val(longitude);
				
				var titleString = address + " Latitude: " + latititude + " Longitude: " + longitude;
				//$("#position").html(titleString);
				//map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map : map,
					position: pos,
					title: titleString
				});
			} else {
				alert('Geocode error: ' + status);
			}
		});
	}


	return GeoCode;
})();

/*
$(document).ready(function() {"use strict";
	var geoCode = new GeoCode(47.6062095, -122.3320708);
	$("#getDirections").click(geoCode.getDirections);

});
*/
