var express = require('express');
var app = express();
var fs = require('fs');

var port = process.env.port || 30025;
app.use(express.bodyParser());
var citiesFileName = 'data/cities.json';

app.get('/', function(req, res) {
	var html = fs.readFileSync('public/index.html');
	res.writeHeader(200, {
		"Content-Type" : "text/html"
	});
	res.write(html);
	res.end();
});

app.get('/getCities', function(request, response) {
	console.log("Get Cities called");
	var json = fs.readFileSync(citiesFileName);
	response.send(json);
});

function writeToFile(fileName, json) {
	console.log("writeToFile called");
	fs.writeFile(fileName, json, function(err) {
		if (err) {
			console.log(err);
		} else {
			console.log("JSON saved to " + fileName);
			return {
				"result" : "success"
			};
		}
	});
}

// Use post when you want to send large chunks of data
app.post('/saveCities', function(request, result) {
	console.log("saveCities called");

	if ( typeof request.body == 'undefined') {
		console.log("request.body is not defined. Did you add app.use(express.bodyParser()); at top");
	} else {
		console.log(request.body);
		var details = request.body.details;
		var json = JSON.parse(request.body.data);
		console.log(details);
		json = JSON.stringify(json, null, 4);
		writeToFile(citiesFileName, json);
	}
});

app.get('/putitem', function(request, result) {
	console.log("putitem called");
	
	console.log(request.query.presidentName);
	console.log(request.query.born);
	console.log(request.query.died);
	writeToFile('temp.json', request.query)
	result.send(outcome);
});

app.use(express.static(__dirname + '/public'));

console.log("listening on Port: ", port);
app.listen(port); 