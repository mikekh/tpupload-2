ELF.own.Keyboard = (function() {

	//Flag for unit test
	ELF.run.keyboardTest = null
	
	ELF.run.numberOfMoves = 0;
	var arrowDirection = null;
	
	function Keyboard() {

		//create Property related to keyboard event listener initialized to "off"
		// that we change later
		Object.defineProperty(this, "keyListenerState", {value:'off', writable: true});
		this.toggleKeys(true);
		
		ELF.run.keyboardTest = "Success";
	}
	
	Keyboard.prototype.listenerState = function(){
    	return this.keyListenerState;
    };
    

   
   Keyboard.prototype.toggleKeys = function(turnOn) {
    	if (turnOn) {
    		window.addEventListener('keydown', doKeyDown, true);       			 // arrow keys turned on
    		this.keyListenerState = 'on';										// set the property to on
    		//alert("Keyboard event listener is : " + this.keyListenerState )	
    	} else {
    		window.removeEventListener ('keydown', doKeyDown, true);    		// arrow keys turned off
    		this.keyListenerState = 'off';										// set the property to off
    		//alert("Keyboard event listener is : " + this.keyListenerState )	
    	}
    };
    
    
    //detect arrow keys 
	var doKeyDown = function(evt) {

        switch (evt.keyCode) {
            case 38:
               //$("#debugs").html("Up arrow was pressed");
               
               arrowDirection = "Up arrow was pressed: ";
               if(ELF.run.Y > 0) {
	               if(blockMove(ELF.run.Y - 1, ELF.run.X) === "false"){
	               	 ELF.run.Y-- ;
	               	 displayMoveCount();
	               }
	            }
                break;
            case 40:
                arrowDirection = "Down arrow was pressed: ";
               if(ELF.run.Y < ELF.run.GRID_LEN_Y - 1){
    			   if(blockMove(ELF.run.Y + 1, ELF.run.X) === "false") {
	               	 ELF.run.Y++ ;
	               	 displayMoveCount();
               	   }
               }
                break;
            case 37:
               arrowDirection = "Left arrow was pressed: ";
			   if(ELF.run.X > 0){
                  if(blockMove(ELF.run.Y, ELF.run.X - 1) === "false") {
	               	 ELF.run.X-- ;
	               	 displayMoveCount();
	              }
               }
                break;
            case 39:
               arrowDirection = "Right arrow was pressed: ";
			   if(ELF.run.X < ELF.run.GRID_LEN_X - 1){
				   if(blockMove(ELF.run.Y, ELF.run.X + 1) === "false") {
	               	 ELF.run.X++ ;
	               	 displayMoveCount();
	               }
               }
                break;
        }
    };
    
    
    //check if the Grid Array has blocking square at the location
    var blockMove = function(row, col) {
    	if (ELF.run.gridArray[row][col] === 1){
    		return "true";
    	}
    	else {
    		return "false";
    	}
    };
    
    function displayMoveCount() {
		ELF.run.numberOfMoves++;
		$("#numbMoves").html(arrowDirection + "  Number of Moves: " + ELF.run.numberOfMoves);
		
		if (ELF.run.numberOfMoves % 10 === 0) {
			var randomNumber = Math.floor(Math.random() * 100) + 1;
			if(randomNumber < 15) {
				ELF.run.jsonHeroData.Strenght++;
				$("#strn").html("Player Strength Increased : " + ELF.run.jsonHeroData.Strenght);
			}
		}
		
		if (ELF.run.numberOfMoves % 25 === 0) {
			ELF.run.jsonHeroData.Experience++;
			$("#expr").html("Player Experince Increased :" + ELF.run.jsonHeroData.Experience);
		} 
	}

	return Keyboard;
})();
