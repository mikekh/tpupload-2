/**
 * @author MIKE
 */



var initCouchDB = function(){
	createCouchDB();
	writeGridArrayDB();
 	writeOrcArrayDB();
 	writeHeroTableDB();
 	writeOrcTableDB();
 	attachHtml();
 	
};

var createCouchDB = function() {
		$.ajax({
			type: 'GET',
			url: '/createDatabase',
			dataType: 'json',
			success: function() {
				showDebug('database created');	       					 //created the database
			},
				
			error: function(errorReturn) {
				var responseText = JSON.parse(errorReturn.responseText);
	        	if (responseText.error === "file_exists") {	  //check if error due to database already exists
					showDebug(responseText.message);
					
				}else {
					showDebug(responseText.message);		 //error not due to database already exists
				}
     		}
	   }); 
};


var readOrcdArrayDB = function() {

        $.ajax({
            type : 'GET',
            url : '/readArray',
            dataType : 'json',
            data : path,
            success : function(orc) {
                //save the array
                //ELF.run.npcdArray  = orc;
            },
            error : showError
        });
	};
	

var writeGridArrayDB = function() {

		var recordInfo = {
	    		recordName: 'GridArray',
	    		recordType: 2,
	    		recordValue: JSON.stringify([
						[1,0,0,0,0,0,0,0,0,0,0,0],
						[1,1,1,1,0,0,0,0,0,0,0,0],
						[0,0,0,1,0,0,0,0,0,0,0,0],
						[0,0,0,1,0,0,0,0,0,0,0,0],
						[0,0,0,1,0,0,0,0,0,0,0,0],
						[0,0,0,0,0,0,0,0,0,0,0,0],
						[0,0,0,0,0,0,0,0,1,1,1,1],
						[0,1,0,0,0,0,0,0,0,0,0,1],
						[0,1,0,0,0,0,0,0,0,0,0,1],
						[0,1,1,1,1,1,0,0,0,0,0,1],
						[0,0,0,1,0,0,0,0,0,0,0,1],
						[0,0,0,1,0,0,0,0,0,0,0,1]
					])
	    };

        makeAjaxWriteCalls(recordInfo);
};

var writeOrcArrayDB = function() {

		var recordInfo = {
	    		recordName: 'OrcArray',
	    		recordType: 3,
	    		recordValue: JSON.stringify([
						[0,0,0,3,0,0,2,0,0,0,0,3],
						[0,0,0,0,0,0,0,0,0,0,0,0],
						[0,0,0,0,0,0,0,0,0,0,0,0],
						[0,0,0,0,0,0,0,0,4,0,0,0],
						[0,0,0,0,0,0,0,0,0,0,0,0],
						[0,0,0,0,0,0,0,0,0,0,0,0],
						[0,0,0,0,0,0,0,0,0,0,0,0],
						[0,0,0,2,0,0,0,0,0,0,0,0],
						[0,0,0,0,0,0,0,0,0,0,0,0],
						[0,0,0,0,0,0,0,0,0,2,0,0],
						[0,4,0,0,0,0,0,2,0,0,0,0],
						[0,0,0,0,0,3,0,0,0,0,0,0]
				])
	    };

        makeAjaxWriteCalls(recordInfo);
};
	
var writeHeroTableDB = function() {

		var recordInfo = {
	    		recordName: 'HeroData',
	    		recordType: 0,
		    		recordValue: {
					    "type": "Hero",
					    "Confidence": "0",
					    "Experience": "0",    
					    "Knowldge": "0",
					    "Health": "0",
					    "Moves": "1",
					    "Strenght": "1",
					    "Wisdom": "1"
					}
		}
		
		makeAjaxWriteCalls(recordInfo);
};


var writeOrcTableDB = function() {

		var recordInfo = {
	    		recordName: 'OrcData',
	    		recordType: 1,
		    		recordValue: {
					    "type": "Orc",
					    "Confidence": "0",
					    "Experience": "0",
					    "Knowldge": "0",
					    "Health": "5",
					    "Moves": "1",
					    "Strenght": "3",
					    "Wisdom": "1"
			   }
		}
		
		makeAjaxWriteCalls(recordInfo);
};


 var attachData = {
        "key": "insertConflictHTML",
        "doc": "insertFightOrc.html"  
}

var attachHtml = function() {
        $.ajax({
            type : 'GET',
            url : '/attachHtml',
            data: attachData,
            dataType : 'json',
            success : function(data) {
                showDebug(data.Result);
            },
            error: showError
        });
}


function makeAjaxWriteCalls(recordInfo){
	$.ajax({
            type : 'GET',
            url : '/writeRecordDB',
            dataType : 'json',
            data : recordInfo,
            success : function(orc) {
                //save the array
                ELF.run.npcdArray  = orc;
            },
            error : showError
        });
}	


var showError = function(request, ajaxOptions, thrownError) {
	//showDebug("Error occurred: = " + ajaxOptions + " " + thrownError );
	//howDebug(request.status);
	showDebug(request.statusText);
	//showDebug(request.getAllResponseHeaders());
	showDebug(request.responseText);
};
	
var showDebug = function(textToDisplay) {
        $("#debug").append('<li>' + textToDisplay + '</li>');
};


