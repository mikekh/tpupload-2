var Draw = (function() {

   //Flag for unit test
	ELF.run.drawTest = null;
	
   //size of the canvas in terms of number of of the images 
	ELF.run.GRID_LEN_X = 12;  // X lenght in # images
	ELF.run.GRID_LEN_Y = 12;  // Y length in # images
			
	ELF.run.X = 5;   //x position of the foreground image. Initialized to 5
	ELF.run.Y = 5;   //y position of the foreground image. Initialized to 5	
	
	ELF.run.gridArray = null;
	ELF.run.npcdArray = null;
	//ELF.run.npcdArray = [];
	//ELF.run.gridArray = [];
	
	var context = null;	
	var imageBackGround = new Image();  
	var imageForeGround = new Image();
	var imageOrc1 = new Image();  
	var imageOrc2 = new Image();  
	var imageOrc3 = new Image();  
	
	var RECT_SIZE = 25;   //size of the images 
	var drawTimer = 0;
	
	
	
	function Draw() {
		$("#btnFight").hide();
		$("#btnFight").click(fightOrc);
		
		//readGridArrayJason();			//reading the json grid file
		//readOrcdArrayJason();			//read the json Orc file. Defines the positions for the Orcs
		
		readGridArrayDB();			    //reading the grid record from database
		readOrcdArrayDB();			    //read the Orc file from databaqse. Defines the positions for the Orcs
		
		context = getCanvas();
		loadImages();

		//object that detects arrow keys
		ELF.run.keyBoard = new ELF.own.Keyboard();
		$("#keysListener01").html("keyListenerState Property is: " +  ELF.run.keyBoard.listenerState());

		//set test flag
		ELF.run.drawTest = "Success";
	}
	

    var getAttachedHtml = function() {
        $.ajax({
            type : 'GET',
            url : '/getAttachedHtml',
            dataType : 'html',
            success : function(data) {
                $('#insrtMarker').html(data);
            },
            error: showError
        });
    };
    
	
	function fightOrc () {
	  window.location.href = 'http://localhost:30025/TestScoring';
	}
	
	function loadImages() {
		imageBackGround.src = "imges/cscGarden01.gif";  // load BACKGROUND image 
		imageForeGround.src = "imges/cscGarden02.gif";  // load FOREGROUND image 
		imageOrc1.src = "imges/orc01.jpg";  // load 
		imageOrc2.src = "imges/orc02.jpg";  // load 
		imageOrc3.src = "imges/orc03.jpg";  // load 
	}
	
	var drawGrid= function(){
		drawGridBackground();
		drawOrcs();
		drawMainCharacter();
	};

	//get the array that defines the background FROM DATABASE
	var readGridArrayDB = function() {
        $.ajax({
            type : 'GET',
            url : '/readJson',
            dataType : 'json',
            //data: { 'docName': 'Grid' },
            data: { 'docName': 'GridArray' },
            success : function(data) {
            
            	//var result = data.grid;
            	
                //the use of below depends on ow the data is stored
                // if the store format is JSAON format the you do'nt need to call it
                var result = JSON.parse(data.GridArrayField);
                ELF.run.gridArray  = result;
            },
            error : showError
        });
	};
	
	//get record from DB that has orc positions on the grid ROM DATABASE
	var readOrcdArrayDB = function() {
        $.ajax({
            type : 'GET',
            url : '/readJson',
            dataType : 'json',
            data: { 'docName': 'OrcArray' },
            //data: { 'docName': 'OrcArrayManual' },
            success : function(data) {
                //the use of below  depends on ow the data is stored
                // if the store format is JSAON format the you dont need to call it
                var result = JSON.parse(data.OrcArrayField);
                ELF.run.npcdArray  = result;

            },
            error : showError
        });
	};


	//read the array that defines the background
	var readGridArrayJason = function() {

		 var path = {
            fileName : 'Data/GridArray.json'
        };
        
        $.ajax({
            type : 'GET',
            url : '/readArray',
            dataType : 'json',
            data : path,
            success : function(grid) {
                //save the array
                ELF.run.gridArray  = grid;
            },
            error : showError
        });
	};
	
	//read the array that defines the postions for the orcs
	var readOrcdArrayJason = function() {

		 var path = {
            fileName : 'Data/OrcArray.json'
        };
        
        $.ajax({
            type : 'GET',
            url : '/readArray',
            dataType : 'json',
            data : path,
            success : function(orc) {
                //save the array
                ELF.run.npcdArray  = orc;
            },
            error : showError
        });
	};
	

	
	//loops to draw image grid, grid size controlled by constants GRID_LEN_X and GRID_LEN_Y
	var drawGridBackground = function() {
		for(var yPos = 0; yPos < ELF.run.GRID_LEN_Y; yPos++) {
			for(var xPos = 0; xPos < ELF.run.GRID_LEN_X; xPos++ ){
			
				// check for ForeGroundImage draw position
				if(ELF.run.gridArray[yPos][xPos] === 0) {
				    context.drawImage(imageBackGround, 0, RECT_SIZE, RECT_SIZE, RECT_SIZE, 
						RECT_SIZE * xPos, RECT_SIZE * yPos, RECT_SIZE, RECT_SIZE);
				} else {
					//draw blocking rectangle
					context.fillStyle = "rgb(255,0,0)";
					context.fillRect(RECT_SIZE * xPos, RECT_SIZE * yPos, RECT_SIZE, RECT_SIZE);
				}
			};
		};
	};

	var drawOrcs = function() {
			for(var yPos = 0; yPos < ELF.run.GRID_LEN_Y; yPos++) {
				for(var xPos = 0; xPos < ELF.run.GRID_LEN_X; xPos++ ){
					//value 1 = blocked, skip over that
					if (ELF.run.npcdArray[yPos][xPos] > 1) {
						switch(ELF.run.npcdArray[yPos][xPos]){
							case 2:
								context.drawImage(imageOrc1, 0, RECT_SIZE, RECT_SIZE, RECT_SIZE, 
									RECT_SIZE * xPos, RECT_SIZE * yPos, RECT_SIZE, RECT_SIZE);
							break;
							case 3:
								context.drawImage(imageOrc2, 0, RECT_SIZE, RECT_SIZE, RECT_SIZE, 
									RECT_SIZE * xPos, RECT_SIZE * yPos, RECT_SIZE, RECT_SIZE);
							break;
							case 4:
					 			context.drawImage(imageOrc3, 0, RECT_SIZE, RECT_SIZE, RECT_SIZE, 
									RECT_SIZE * xPos, RECT_SIZE * yPos, RECT_SIZE, RECT_SIZE);
							break;																		
						}
					}
				};
			};
		};

	var drawMainCharacter = function(){
		context.drawImage(imageForeGround, 0, RECT_SIZE, RECT_SIZE, RECT_SIZE, 
		RECT_SIZE * ELF.run.X, RECT_SIZE * ELF.run.Y, RECT_SIZE, RECT_SIZE);	
		
		detectConflict(ELF.run.Y, ELF.run.X);
	};


	//check main character collides with an orc
    var detectConflict = function(row, col) {
    	if(ELF.run.npcdArray[row][col] > 1){
    		
			//Insert TestScoring page at #conflictDiv in this page
    		$.ajax({
			type: 'GET',
			url: '/TestScoring',
			dataType: 'html',
			success: function(data) {
				$('#conflictDiv').html(data);
				$("#heroTbl").css({ backgroundColor : "yellow"});
				$("#orcTbl").css({ backgroundColor : "red"});
				$("#btnFight").show();
				getAttachedHtml();
				
				
				// just show tables and hide the rest
				//come back to this when .load() works
				$('#testConflictAnchor').hide();
				$("#testConflict").hide();				
				$("#btnRead").hide();
				$("#btnWrite").hide();
				$("#btnStrike").hide();
				$('#testGridAnchor').hide();
			},
			error: showError      
		    }); 
    	}
    };
    
    
	//get the canvas CONTEXT, it is the object on which the drawing actually done
	var getCanvas = function() {
		var canvas = document.getElementById('mainCanvas');
		if (canvas !== null) {
			var context = canvas.getContext('2d'); 
			drawTimer = setInterval(drawGrid, 50);   //set timer
			return context;
		} else {
			$("#debugs").css({
				backgroundColor : "blue",
				color: "red"
			});
			$("#debugs").html("Could not retrieve canvas");
			return null;
		}
	};
	
	var showError = function(request, ajaxOptions, thrownError) {
	showDebug("Error occurred: = " + ajaxOptions + " " + thrownError );
	showDebug(request.status);
	showDebug(request.statusText);
	showDebug(request.getAllResponseHeaders());
	showDebug(request.responseText);
	};
	
	var showDebug = function(textToDisplay) {
        $("#debug").append('<li>' + textToDisplay + '</li>');
    };
	
	return Draw;
})();



$(document).ready(function() {'use strict';
	new Draw();
});
