/**
 * @author MIKE
 */

var TestScoring = (function() {

	 //Flag for unit test
	ELF.run.ConflictTest = null;

	var jsonHeroData = null;
	var jsonOrcData = null;
	var ajaxData = null;
	
	function TestScoring() {
		$("#btnRead").empty();
		$("#btnRead").hide();
		$("#orcCanvas").hide();
		
		$("#btnWrite").click(writeJsonFiles);
		$("#btnStrike").click(genRandNumb);
		
		
		
		//readJsonFiles();
		readTablesDB();
		
		//set test flag
		ELF.run.ConflictTest = "Success";
	}
	
	
	function updateTable2(tableType, jsonObj){
		//alert("in updateTable");
		switch (tableType){
			case 'Hero':
				updateTable ("#heroTbl", jsonObj);
				jsonHeroData = jsonObj;
			break;
			case 'Orc':
				updateTable ("#orcTbl", jsonObj);
				jsonOrcData = jsonObj;
			break;
		
		}
	}
	
	function updateTable(tableID, jsonObj){
		//alert("in updateTable");
		
		var table = $(tableID);
		table.empty();
		for (var row in jsonObj) {
				//alert("creating rows in tabled");
				table.append("<tr><td>" + row + "</td><td id=" + row.toLowerCase() + ">" + jsonObj[row] + "</td></tr>");
			}
	}
	
	
	function readTablesDB() {
		ajaxReadDB('HeroData');
		ajaxReadDB('OrcData');
	}
	
	
	var writeJsonFiles = function() {
		//writeJsonHero();
		//writeJsonOrc();
		writeCurrentOrcTableDB(jsonOrcData);
		writeCurrentHeroTableDB(jsonHeroData);
		ajaxWrite('/writeHero', jsonHeroData);
		ajaxWrite('/writeOrc',jsonOrcData);
	}
	
//write the characteristic data of Hero to DB
var writeCurrentHeroTableDB = function(jsonHeroData) {
	var recordInfo = null;
		 recordInfo = { 
	    		recordName: 'HeroData',
	    		recordType: 1,
		    		recordValue: {
					    "type": jsonHeroData.type,
					    "Confidence": jsonHeroData.Confidence,
					    "Experience": jsonHeroData.Experience,
					    "Knowldge": jsonHeroData.Knowldge,
					    "Health": jsonHeroData.Health,
					    "Moves": jsonHeroData.Moves,
					    "Strenght": jsonHeroData.Strenght,
					    "Wisdom": jsonHeroData.Wisdom,
					    
			   }
		}
		
		makeAjaxWriteCalls(recordInfo);
};	


//write the characteristic data of Orc to DB
var writeCurrentOrcTableDB = function(jsonOrcData) {
	var recordInfo = null;
		 recordInfo = { 
	    		recordName: 'OrcData',
	    		recordType: 1,
		    		recordValue: {
					    "type": jsonOrcData.type,
					    "Confidence": jsonOrcData.Confidence,
					    "Experience": jsonOrcData.Experience,
					    "Knowldge": jsonOrcData.Knowldge,
					    "Health": jsonOrcData.Health,
					    "Moves": jsonOrcData.Moves,
					    "Strenght": jsonOrcData.Strenght,
					    "Wisdom": jsonOrcData.Wisdom,
					    
			   }
		}
		
		makeAjaxWriteCalls(recordInfo);
};


function makeAjaxWriteCalls(recordInfo){
	$.ajax({
            type : 'GET',
            url : '/writeRecordDB',
            dataType : 'json',
            data : recordInfo,
            success : function(orc) {
                $("#debug").append('<li>Record written to DB Successfully</li>');
            },
            error : showError
        });
}	

	// read DB record for recordName passed in
	function ajaxReadDB(recordName) {
		
		//alert("readJsonHero called");
		 $.ajax({
            type : 'GET',
            url : '/readJson',
            dataType : 'json',
            //data: { 'docName': 'Hero' },
            data: { 'docName': recordName },
            success : function(data) {
                //var tableData = data.statistics;
                //type = data.statistics.type;
                
                var tableData = data.TableField;
                var type = tableData.type;
				updateTable2 (type, tableData);
            },
            error : showError
        });;
	};
	

	
	function ajaxWrite(inPramURL,jsonObj) {
		//alert("writeJsonHero called");
		
			$.ajax({
			type: 'GET',
			url: inPramURL,
			dataType: 'json',
			data: jsonObj, 
			success: function(data) {
				showDebug(data.result);
			},
			error: showError      
		});
	};
	
	function genRandNumb(){
		//alert("strike clicked");
		
		var playerBonus = 0;
		var MAX_NUMBER  = 100; 
		var randomnumber=Math.floor(Math.random() * (MAX_NUMBER + 1));
		
		if(jsonOrcData.Health < 1){
			orcIsDead();
			$("#randNumbList").empty();
			return;				//nothing to strike the ORC is dead
		}
		else{
			$("#randNumbList").append('<li>' + "Random Number: " + randomnumber + '</li>');
		}
	
	  	//calculate players bonus 
	  	//playerBonus = jsonHeroData.Experience + jsonHeroData.Strenght + randomnumber;
	  	playerBonus =  randomnumber;
		dislayChange(playerBonus);
		
	  	if(playerBonus > 50){
	  		//update tables
	  		jsonOrcData.Health -= 1;
	  		updateTable ("#orcTbl", jsonOrcData);
	  		jsonHeroData.Experience++;
	  		updateTable ("#heroTbl", jsonHeroData);
	  		
	  		if(jsonOrcData.Health < 1){
				orcIsDead(); 
				$("#randNumbList").empty(); 
			}
	  	} 
	}
	
	function dislayChange(playerBonus){
		if(playerBonus > 50){
	  		$("#exper").html("Player experince has Increases");
	  		$("#hlth").html("Orc Health has Decreased")
	  		$("#bonus").html("Player bonus :" + playerBonus);
	  	} else {
	  		$("#exper").html("Player experince UnChanged");
	  		$("#hlth").html("Orc Health has UnChanged");
	  		$("#bonus").html("Player bonus : 0");
	  	}
	}
	
	function orcIsDead(){
		//alert('ORC DEAD!!');

		$.ajax({
			type: 'GET',
			url: '/TestCanvas',
			dataType: 'html',
			success: function(data) {
				//$('#orcCanvas').empty();
				$('#orcCanvas').html(data);
				$('#orcCanvas').show();
				$("#btnRead").hide();
				$("#btnStrike").hide();
				writeJsonFiles();
				
			},
			error: showError      
		    }); 
	}
		
	
var showError = function(request, ajaxOptions, thrownError) {
	//showDebug("Error occurred: = " + ajaxOptions + " " + thrownError );
	//howDebug(request.status);
	showDebug(request.statusText);
	//showDebug(request.getAllResponseHeaders());
	showDebug(request.responseText);
};
	
var showDebug = function(textToDisplay) {
        $("#debug").append('<li>' + textToDisplay + '</li>');
};
	return TestScoring;

})();

$(document).ready(function() {
	//$("#works").html("IT WORKS");
	new TestScoring();
});

