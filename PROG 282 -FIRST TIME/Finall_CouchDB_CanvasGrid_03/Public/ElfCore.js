// The only item our program puts in the global namespace
var ELF = {};

// This will hold declarations of object 
ELF.own = {
	numberOfMoves : 0
};

// This object will hold all the instantiated objects
// IE when you do a 'new" to create an object.
// These object will be are available throughout the program.
ELF.run = {};

