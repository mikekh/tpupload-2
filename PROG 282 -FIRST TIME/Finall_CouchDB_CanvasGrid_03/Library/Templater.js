/**
 * @author Mike khan
 */

var fs = require('fs');
var handlebars = require('handlebars');

var Templater = (function() {
	'use strict';

	function Templater() {
	}

	// Please not that we convert to a string.
	var readHtml = function(fileName) {
		return String(fs.readFileSync(fileName));
	};

	Templater.prototype.addTable1 = function(fileName, tableName, tableName2) {
		var mainFile = readHtml(fileName);
		var table = readHtml(tableName);
		var table2 = readHtml(tableName2);

		var template = handlebars.compile(mainFile);

		var result = template({
			heroTbl : table,
			orcTbl : table2
		});

		return result;
	};

	Templater.prototype.addTable2 = function(fileName, tableName) {
		var mainFile = readHtml(fileName);
		var table = readHtml(tableName);

		var template = handlebars.compile(mainFile);

		var result = template({
			orcTbl : table
		});

		return result;
	};
	return Templater;

})();

exports.template = new Templater()
